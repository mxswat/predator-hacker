_G.MenuMx_hacker = _G.MenuMx_hacker or {}
MenuMx_hacker._path = ModPath
MenuMx_hacker._data_path = ModPath .. "/save/MenuMx_hacker_options.txt"
MenuMx_hacker._data = {}
--[[
    okay, so, add your skills ids from the skilltreetweakdata in the banned_skill_couple var
    
    you have to add the two skill ids like in the example below
]]
MenuMx_hacker.banned_skill_couple = {
	["tower_defense"] = "less_is_better"
}
-- If you want you can localize the text but you can leave it empty or like it is now
MenuMx_hacker.banned_skill_loc = {
    ["tower_defense"] = "TOWER DEFENSE",
    ["less_is_better"] = "LESS IS BETTER"
}

-- I revert the table to make the search faster
local s={}
for k,v in pairs(MenuMx_hacker.banned_skill_couple) do
    s[v]=k
end
MenuMx_hacker.banned_skill_couple_revert = s

function MenuMx_hacker:get_data()
	return MenuMx_hacker._data.Mx_hacker_val
end

function MenuMx_hacker:Save()
	local file = io.open( self._data_path, "w+" )
	if file then
		file:write( json.encode( self._data ) )
		file:close()
	end
end
function MenuMx_hacker:Load()
	local file = io.open( self._data_path, "r" )
	if file then
		self._data = json.decode( file:read("*all") )
		file:close()
		if self._data.Mx_hacker_val == nil then self._data.Mx_hacker_val = true end
		MenuMx_hacker:Save()
	end
end
Hooks:Add("MenuManagerInitialize", "MenuMx_hacker_MenuManagerInitialize", function(menu_manager)
	MenuCallbackHandler.MenuMx_hacker_save = function(self, item)
		MenuMx_hacker:Save()
	end	
	MenuCallbackHandler.Mx_hacker_func = function(self, item)
		MenuMx_hacker._data.Mx_hacker_val = (item:value() == "on" and true or false)
	end		
	MenuMx_hacker:Load()
	MenuHelper:LoadFromJsonFile(MenuMx_hacker._path .. "Hooks/options.txt", MenuMx_hacker, MenuMx_hacker._data)
end )