_G.MenuMx_hacker = _G.MenuMx_hacker or {}
--[[
    This code will care about associate your banned skills
]]
Hooks:PostHook(NewSkillTreeGui, "invest_point", "invest_point_mod", function(self, item)
	local skill_id = item:skill_id()
	local unlocked = self._skilltree:skill_unlocked(nil, skill_id)
	    
	if not unlocked then -- If it's now unlocked i dont need to check lol
		return
    end

    function check_banned_skill_couple(currentItem, found_id)
        if (self._skilltree:next_skill_step(found_id) >= 2) then
            self:refund_point(item) -- I refound you the cost of the skill
            local currentSkillLoc = MenuMx_hacker.banned_skill_loc[currentItem:skill_id()] or tostring(currentItem)
            local foundSkillLoc = MenuMx_hacker.banned_skill_loc[found_id] or tostring(found_id)
            local menu = QuickMenu:new( "Sentry Rework", "You can't use ".. currentSkillLoc .. " with " .. foundSkillLoc, {} )
            menu:Show()
        end
    end

    if MenuMx_hacker.banned_skill_couple[skill_id] then 
        check_banned_skill_couple(item, MenuMx_hacker.banned_skill_couple[skill_id])
    end
    if MenuMx_hacker.banned_skill_couple_revert[skill_id] then
        check_banned_skill_couple(item, MenuMx_hacker.banned_skill_couple_revert[skill_id])
    end
    return
end)

